require('jquery');
require('popper.js');
require('bootstrap');
const fs = require('fs');
const { dialog } = require('electron').remote;

function parseTitle(title) {
  const context = document.createElement('canvas').getContext('2d');
  context.font = "120px Arial";
  const splitTitle = title.split(' ');
  let current = "";
  let result = [];
  for (i in splitTitle) {
    const test = current.concat(`${splitTitle[i]} `);
    if (context.measureText(test).width > 2350) {
      result.push(current.trim());
      current = `${splitTitle[i]} `;
    } else {
      current = test;
    }
  }
  if (current.length > 0) {
    result.push(current.trim());
  }

  return result;
}

function createImage(title, organization, spine, color) {
  const colors = {
    Acapulco: '#2B6365',
    Trinidad: '#6F1E0F',
    EastBay: '#371844',
    Atlantis: '#134125'
  };

  const spinePixels = parseFloat(spine) * 300;
  const cover = document.createElement('canvas');
  const context = cover.getContext('2d');
  //const logo = fs.readFileSync('./public/images/logo.png');
  
  // setup cover base
  context.canvas.width = 5175 + spinePixels;
  context.canvas.height = 3375;
  context.beginPath();
  context.rect(0, 0, context.canvas.width, context.canvas.height);
  context.fillStyle = colors[color.replace(' ', '')];
  context.fill();

  // add background images
  const backgroundImage = document.getElementById(color.replace(' ', '-').toLowerCase());
  context.drawImage(backgroundImage, 18.75, 37.5, 2550, 3300);
  context.drawImage(backgroundImage, (2587.5 + spinePixels), 37.5, 2550, 3300); 

  // add back text
  context.beginPath();
  context.font = "90px Arial";
  context.textAlign = "center";
  context.fillStyle = "#FFFFFF";
  context.fillText("Find great free", 1293.75, 2250);
  context.fillText("psychology content at", 1293.75, 2370);
  context.fillText("www.nobaproject.com", 1293.75, 2490);

  // add back logo
  const logo = document.getElementById('logo');
  context.drawImage(logo, 993.75, 2640, 600, 184.5);
  
  // add front text
  const parsedTitle = parseTitle(title);
  let top = 2250;

  context.font = "120px Arial";
  for (i in parsedTitle) {
    context.fillText(parsedTitle[i], (3881.25 + spinePixels), top);
    top += 160;
  }

  top -= 100;
  
  // add front logo
  context.drawImage(logo, (3581.75 + spinePixels), top, 600, 184.5);

  top += 300;
  context.font = "90px Arial";
  context.fillText(organization, (3881.25 + spinePixels), top);
  
  // add spine text
  if (spinePixels > 150) {
    context.textAlign = "left";
    context.rotate(Math.PI/2);
    context.fillText(title, 100, -(2587.5 + spinePixels/2 - 60));
    context.rotate(-Math.PI/2);
  }
  
  const coverData = cover.toDataURL('image/jpeg').replace(/^data:image\/\w+;base64,/, "");
  return new Buffer(coverData, 'base64');
}

function saveFile(event) {
  event.preventDefault();

  const title = document.getElementById('title').value;
  const organization = document.getElementById('organization').value;
  const spine = document.getElementById('spine').value;
  const color = document.getElementById('color').value;

  const data = createImage(title, organization, spine, color);
  const options = {
    defaultPath: title.replace(' ', '_').replace(/(:|;)/, '_'),
    filters: [{
      name: "Noba_Cover",
      extensions: ['jpg']
    }]
  };
  
  dialog.showSaveDialog(options, (savePath) => {
    if (savePath !== null && savePath.length > 0) {
      fs.writeFile(savePath, data, (err) => {
        if (err) dialog.showErrorBox('Failed to save', err.message);
      });
    }
  });
}

document.addEventListener('DOMContentLoaded', () => {
  const saveButton = document.getElementById('save');
  saveButton.addEventListener('click', saveFile);
});
